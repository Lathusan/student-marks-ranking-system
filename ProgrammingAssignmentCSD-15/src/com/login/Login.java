package com.login;

import java.util.Scanner;

import com.usersFunctions.Admin;
import com.usersFunctions.Teachers;

public class Login {

	public void login(String userName, String password) {

		Scanner scan = new Scanner(System.in);
		Login logincall = new Login();

		String adminU = "admin";
		String adminP = "admin";
		String teacherU = "teacher01";
		String teacherP = "teacher01";
		String studentU = "student01";
		String studentP = "student01";

		int type;
		/*
		 * int admin = 1; int teacher = 2; int student = 3; int exit = 4;
		 */

		System.out.println("      Log in Type");
		System.out.println("   =================");
		System.out.println("1. Admin");
		System.out.println("2. Teacher");
		System.out.println("3. Student");
		System.out.println("4. Exit");
		System.out.print("Select a Number : ");
		type = scan.nextInt();
		System.out.println("");
		
		if (type == 4) {
			System.out.println("   Good Bye!!!");
			System.exit(0);
		}
		
		System.out.println("      Log in");
		System.out.println("   ============");
		System.out.print("User Name: ");
		userName = scan.next();

		System.out.print("Password : ");
		password = scan.next();

		if (type == 1) {

			// Admin
			if (userName.equals(adminU) && password.equals(adminP)) {
				System.out.println("\nHello Admin!!!");
				Admin admin1 = new Admin();
				admin1.admin1();

			} else {
				System.err.println("Incorrect Username or Password!!!");
				logincall.login(userName, password);
			}

		} else if (type == 2) {
			// Teacher
			if (userName.equals(teacherU) && password.equals(teacherP)) {
				System.out.println("\nHello Teacher!!!");
				Teachers teachers = new Teachers();
				teachers.teacher1();

			} else {
				System.err.println("Incorrect Username or Password!!!");
				logincall.login(userName, password);
			}

		} else if (type == 3) {
			// Student
			if (userName.equals(studentU) && password.equals(studentP)) {
				System.out.println("\nHello Student!!!");

			} else {
				System.err.println("Incorrect Username or Password!!!");
				logincall.login(userName, password);
			}
		}

	}

}

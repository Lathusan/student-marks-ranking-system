package com.teacher;

import java.util.Scanner;

import com.usersFunctions.Teachers;

public class TeacherOption {

	public void addStudent() {

		Scanner scan = new Scanner(System.in);

		String stu[] = new String[100];
		String stuGrade[] = new String[100];

		stu[0] = "Raja";
		stu[1] = "Mala";

		stuGrade[0] = "6A";
		stuGrade[1] = "8C";

		System.out.print("How many student add? : ");
		int enterTimes = scan.nextInt();

		for (int i = 2; i < (enterTimes + 2); i++) {
			System.out.print("Enter student name : ");
			stu[i] = scan.next();
			System.out.print("Enter student grade : ");
			stuGrade[i] = scan.next();
		}

		for (int j = 0; j < (enterTimes + 2); j++) {
			System.out.println(stu[j] + "\t" + stuGrade[j]);
		}

		System.out.println("Do you want to add another student? \n 1.yes \n 2.No");
		int stuAddOpp = scan.nextInt();

		switch (stuAddOpp) {
		case 1:
			TeacherOption add = new TeacherOption();
			add.addStudent();
			break;
		default:
			Teachers teacher = new Teachers();
			teacher.teacher1();
			System.out.println("");
		}

	}

	public void removeStudent() {

		Scanner scan = new Scanner(System.in);

		String stu[][] = new String[3][2]; // = new String[100]; // Sir in my Error is [3][3]
		// String stuGrade[] = {};// = new String[100];

		stu[0][0] = "Stu1";
		stu[0][1] = "6A";

		stu[1][0] = "Stu2";
		stu[1][1] = "7B";

		stu[2][0] = "Stu3";
		stu[2][1] = "9C";

		/*
		 * stuGrade[0] = "6A"; stuGrade[1] = "8C"; stuGrade[2] = "7D";
		 */
		System.out.println("\nAvailable class students ");
		for (int j = 0; j < 3; j++) {
			System.out.print((j + 1) + ". ");
			for (int i = 0; i < 2; i++) {
				System.out.print(stu[j][i] + "\t");
			}
			System.out.println("");
		}

		System.out.print("Select remove student ID NO? : ");
		int stuIDNo = scan.nextInt();

		switch (stuIDNo) {
		case 1:
			stu[0][0] = "---";
			stu[0][1] = "---";
			break;
		case 2:
			stu[1][0] = "---";
			stu[1][1] = "---";
			break;
		case 3:
			stu[2][0] = "---";
			stu[2][1] = "---";
			break;
		default:
			break;
		}

		System.out.println("Name " + "  Grade");

		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 2; i++) {
				System.out.print(stu[j][i] + "\t");
			}
			System.out.println("");
		}
		
		System.out.println("");
		
		Teachers teacher = new Teachers();
		teacher.teacher1();
		System.out.println("");

	}

}

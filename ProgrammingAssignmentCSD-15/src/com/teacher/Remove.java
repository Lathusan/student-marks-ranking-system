package com.teacher;

import java.util.Scanner;

public class Remove {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		String stu[][] = new String[3][2]; 
		// String stuGrade[] = {};// = new String[100];

		stu[0][0] = "Stu1";
		stu[0][1] = "6A";

		stu[1][0] = "Stu2";
		stu[1][1] = "7B";

		stu[2][0] = "Stu3";
		stu[2][1] = "9C";

		/*
		 * stuGrade[0] = "6A"; stuGrade[1] = "8C"; stuGrade[2] = "7D";
		 */

		for (int j = 0; j < 3; j++) {
			System.out.print((j + 1) + ". ");
			for (int i = 0; i < 2; i++) {
				System.out.print(stu[j][i] + "\t");
			}
			System.out.println("");
		}

		System.out.print("Select remove student ID NO? : ");
		int stuIDNo = scan.nextInt();

		switch (stuIDNo) {
		case 1:
			stu[0][0] = "---";
			stu[0][1] = "---";
			break;
		case 2:
			stu[1][0] = "---";
			stu[1][1] = "---";
			break;
		case 3:
			stu[2][0] = "---";
			stu[2][1] = "---";
			break;
		default:
			break;
		}

		System.out.println("Name " + "  Grade");

		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 2; i++) {
				System.out.print(stu[j][i] + "\t");
			}
			System.out.println("");
		}

	}

}

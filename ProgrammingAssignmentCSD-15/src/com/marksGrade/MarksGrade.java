package com.marksGrade;

import java.util.Scanner;

public class MarksGrade {

	public void markgrade() {

		Scanner scanCal = new Scanner(System.in);
		MarksGrade marksGrade = new MarksGrade();

		double sub[] = { 0, 0, 0 };

		System.out.print("Enter the subject 1 marks : ");
		sub[0] = scanCal.nextDouble();
		System.out.print("Enter the subject 2 marks : ");
		sub[1] = scanCal.nextDouble();
		System.out.print("Enter the subject 3 marks : ");
		sub[2] = scanCal.nextDouble();

		for (int i = 0; i < sub.length; i++) {
			// System.out.println(sub[i]);

			if (100 >= sub[i] && sub[i] >= 75) {
				System.out.println("Subject " + (i + 1) + " : " + sub[i] + "  A");

			} else if (74 >= sub[i] && sub[i] >= 65) {
				System.out.println("Subject " + (i + 1) + " : " + sub[i] + "  B");

			} else if (64 >= sub[i] && sub[i] >= 55) {
				System.out.println("Subject " + (i + 1) + " : " + sub[i] + "  C");

			} else if (54 >= sub[i] && sub[i] >= 35) {
				System.out.println("Subject " + (i + 1) + " : " + sub[i] + "  D");

			} else if (34 >= sub[i] && sub[i] >= 0) {
				System.out.println("Subject " + (i + 1) + " : " + sub[i] + "  F");

			} else {
				System.out.println("please enter the valide marks.");
				// marksGrade.marksCal();

			}
		}

		// change dynamic
		double total = sub[0] + sub[1] + sub[2];
		double avarage = total / 3;

		System.out.println("\nTotal     : " + total);
		System.out.println("Avarage   : " + avarage);
	}

}

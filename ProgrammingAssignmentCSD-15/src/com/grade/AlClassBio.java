package com.grade;

import java.util.Scanner;
import java.util.Stack;

import org.w3c.dom.css.Counter;

import com.marksGrade.*;
import com.usersFunctions.Teachers;

public class AlClassBio {

	public void alBio() {
		
		Scanner scan = new Scanner(System.in);
		MarksGrade grade = new MarksGrade();
		
		Teachers teachers = new Teachers();

		String name[] = new String[30];
		double sub[][] = new double[30][3];
		String subject[] = {"BIO", "CHE", "PHY"};
		// double sub1[] = new double[30];
		// double sub2[] = new double[30];
		// double sub3[] = new double[30];

		String result = null;
		double total = 0;
		double avarage = 0;
		double twodigava = 0;

		name[0] = "Kannan";
		name[1] = "Malini";
		name[2] = "Vinoth";

		sub[0][0] = 62;
		sub[0][1] = 80;
		sub[0][2] = 74;

		sub[1][0] = 55;
		sub[1][1] = 44;
		sub[1][2] = 65;

		sub[2][0] = 68;
		sub[2][1] = 54;
		sub[2][2] = 40;

		/*
		 * sub1[0] = 62; sub1[1] = 80; sub1[2] = 74;
		 * 
		 * sub2[0] = 55; sub2[1] = 69; sub2[2] = 65;
		 * 
		 * sub3[0] = 68; sub3[1] = 54; sub3[2] = 40;
		 */
		try {
			System.out.println("School : Jaffna Hindu College");
			System.out.println("Grade  : A/L Biology 12A\n"); 
			System.out.println("Name \tSubject  Marks \tResult");
			System.out.println("==============================");

			// for (int k = 0; k < name.length; k++) {
			// System.out.print(name[k]);
			for (int i = 0; i < sub.length; i++) {
				System.out.print(name[i] + "  ");
				for (int j = 0; j < sub[i].length; j++) {

					if (name[i].equals(null)) {
						break;
					}
					System.out.print("  " + subject[j] + "   ");
					// return sub1[s1] + sub2[s2] + sub3[s3];

					if (100 >= sub[i][j] && sub[i][j] >= 75) {
						// System.out.println(/* "Subject " + (i + 1) + " : " + sub[i] + */" A");
						result = "A";
					} else if (74 >= sub[i][j] && sub[i][j] >= 65) {
						// System.out.println(/* "Subject " + (i + 1) + " : " + sub[i] + */ " B");
						result = "B";
					} else if (64 >= sub[i][j] && sub[i][j] >= 55) {
						// System.out.println(/* "Subject " + (i + 1) + " : " + sub[i] + */" C");
						result = "C";
					} else if (54 >= sub[i][j] && sub[i][j] >= 35) {
						// System.out.println(/* "Subject " + (i + 1) + " : " + sub[i] + */" D");
						result = "D";
					} else if (34 >= sub[i][j] && sub[i][j] >= 0) {
						// System.out.println(/* "Subject " + (i + 1) + " : " + sub[i] + */" F");
						result = "F";
					} else {
						System.out.println("please enter the valide marks.");
						// marksGrade.marksCal();
					}
					// System.out.print(name[j]);
					System.out.print(" " + sub[i][j] + "\t" + "  " + result + "\n\t");

					double sub1 = sub[i][0];
					double sub2 = sub[i][1];
					double sub3 = sub[i][2];
					total = (sub1 + sub2 + sub3);
					avarage = (total/3);	//In future change dynamic i
					twodigava = (int)(Math.round(avarage * 100))/100.0;
					
				}
				System.out.println("  =================");

				System.out.println("Total\t\t" + total);
				System.out.println("Avarage\t\t " + twodigava);

				System.out.println("\t  =================\n");

			}
			// }

		} catch (Exception e) {
			// System.out.println("Null Point Exception");
		}
		System.out.println("");
		teachers.teacher1();
	}
}
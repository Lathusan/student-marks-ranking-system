package com.usersFunctions;

import java.util.Scanner;

import com.grade.AlClassBio;
import com.teacher.TeacherOption;

public class Teachers {

	public void teacher1() {

		Scanner scanTeacher = new Scanner(System.in);

		System.out.println("1. Add student.");
		System.out.println("2. Remove student.");
		System.out.println("3. Enter student marks.");
		System.out.println("4. View one student marks.");
		System.out.println("5. View whole student marks.");
		System.out.println("6. Log Out.");
		System.out.print("Select an option : ");
		
		TeacherOption option = new TeacherOption();
		AlClassBio classBio = new AlClassBio();
		
		int i = scanTeacher.nextInt();
		switch (i) {
		case 1:
			option.addStudent();
			break;
		case 2:
			option.removeStudent();
			break;
		case 3:
			System.out.println("Not functions here.");
			break;
		case 4:
			classBio.alBio();
			break;
		case 5:
			System.out.println("Not functions here.");
			break;
		case 6:
		//	System.out.println("Log Out !!!");
			System.exit(0);
			break;
		default:
			System.err.println("Option not selected.\n");
			Teachers teachers = new Teachers();
			teachers.teacher1();
		}
	}

}
